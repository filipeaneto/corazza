#include "handler.h"
#include "command.h"

#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

Status *hstatus = NULL;

void handler_set_status(Status *status)
{
    hstatus = status;
}

void chandler(int signal)
{
    (void) signal;

    int pid = status_get_next_job(hstatus, 1);
    int child_status;

    while(pid)
    {
        if(waitpid(pid, &child_status, WNOHANG | WSTOPPED | WCONTINUED) > 0)
        {
            if(WIFSTOPPED(child_status))
            {
                /*            printf("%d recebeu stop\n", pid);*/
                status_set_job_status(hstatus, pid, "Stopped");
            }
            else if(WIFEXITED(child_status) || WIFSIGNALED(child_status) )
            {
                /*            printf("%d parou.\n", pid);*/
                status_finish_job(hstatus, pid);
            }
            else if(WIFCONTINUED(child_status))
            {
                /*            printf("%d continuou\n", pid);*/
                status_set_job_status(hstatus, pid, "Running");
            }
        }

        pid = status_get_next_job(hstatus, 0);
    }
}

void inthandler(int signal)
{
    (void) signal;

    if(status_is_active(hstatus))
    {
        printf("\n[%s]$ ", status_get_current(hstatus));
        fflush(stdout);
    }
}

void stphandler(int signal)
{
    (void) signal;

    if(status_is_active(hstatus))
    {
        printf("\n[%s]$ ", status_get_current(hstatus));
        fflush(stdout);
    }
}

