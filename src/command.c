#include "command.h"
#include "utils.h"

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/signal.h>

char *cmd_help(Status *status)
{
    (void) status;

    return "Corazza shell, version 0.1\n"
           "To run a external command: $ command [args] [< in] [> out | >> out ]\n"
           "in and out are path to files\n\n"
           "To run a command with PIPE: $ command 1 | command2 [| command3 ...]\n\n"
           "To create a job: $ command &\n"
           "To make job background: ^Zn\n"
           "To run a stopped background job: bg and the number of the job\n"
           "To make job foreground: fg and the number of the job, Ex: $ fg 1\n\n"
           "help - show this message\n"
           "exit - exit the shell\n";
}

void cmd_exit(Status *status)
{
    status_set_running(status, 0);
}

