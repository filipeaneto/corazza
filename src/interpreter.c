#include "interpreter.h"
#include "handler.h"
#include "command.h"
#include "parser.h"
#include "status.h"
#include "utils.h"

#include <sys/signal.h>
#include <sys/types.h>      /* wait() */
#include <sys/wait.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>     /* fork() */
#include <stdio.h>
#include <fcntl.h>

typedef struct
{
    char **args;
    char *input;
    char *output;
    short cat;
} cmd_t;

int internal_cmd_builtin2(Status *status, cmd_t *cmd)
{
    if(strcmp(cmd->args[0], COMMAND_EXIT) == 0)
    {
        cmd_exit(status);
        return 1;
    }
    else if(strcmp(cmd->args[0], COMMAND_CD) == 0)
    {
        char path[STATUS_MAX_CMD_SIZE];
        strcpy(path, cmd->args[1]);

        int i;
        for(i = 2; cmd->args[i] != NULL; i++)
        {
            strcat(path, " ");
            strcat(path, cmd->args[i]);
        }

        status_set_current(status, path);
        return 1;
    }
    else if(strcmp(cmd->args[0], COMMAND_FG) == 0)
    {
        int index;
        if(cmd->args[1] == NULL)
            index = 0;
        else
            sscanf(cmd->args[1], "%d", &index);
        int pid = status_job_foreground(status, index);

        if(pid > 0)
        {
            int child_status;

            tcsetpgrp(STDIN_FILENO, pid);

            fflush(stdout);

            status_set_active(status, 0);
            waitpid(pid, &child_status, WSTOPPED);
            status_set_active(status, 1);

            /* Filho foi parado */
            if(WIFSTOPPED(child_status))
            {
                status_set_job_status(status, pid, "Stopped");
            }
            else
            {
                status_finish_job(status, pid);
            }

            tcsetpgrp(STDIN_FILENO, status_get_pid(status));
        }

        return 1;
    }
    else if(strcmp(cmd->args[0], COMMAND_BG) == 0)
    {
        int index;
        if(cmd->args[1] == NULL)
            index = 0;
        else
            sscanf(cmd->args[1], "%d", &index);
        status_job_background(status, index);

        return 1;
    }

    return 0;
}

int internal_cmd_builtin(Status *status, cmd_t *cmd)
{
    int ret = 0;

    FILE *fp = stdout;

    if(cmd->output != NULL)
    {
        fp = fopen(cmd->output, cmd->cat == 1 ? "a" : "w");
    }
    else if(strcmp(cmd->args[0], COMMAND_HELP) == 0)
    {
        fprintf(fp, "%s", cmd_help(status));
        ret = 1;
    }
    else if(strcmp(cmd->args[0], COMMAND_JOBS) == 0)
    {
        status_print_jobs(status, fp);
        ret = 1;
    }


    if(cmd->output != NULL)
    {
        fclose(fp);
    }

    return ret;
}

void internal_cmd_external(Status *status, cmd_t *cmd)
{
    (void) status;

    int fd;

    if(cmd->output != NULL)
    {
        fd = open(cmd->output, O_CREAT | O_RDWR | (cmd->cat ? O_APPEND : 0),
                  S_IRUSR | S_IWUSR);

        if(fd < 0)
        {
            fatal();
        }

        close(1);

        if(dup(fd) < 0)
        {
            fatal();
        }

        close(fd);
    }

    if(cmd->input != NULL)
    {
        fd = open(cmd->input, O_RDONLY, S_IRUSR);

        if(fd < 0)
        {
            fatal();
        }

        close(0);

        if(dup(fd) < 0)
        {
            fatal();
        }

        close(fd);
    }

    execvp(cmd->args[0], cmd->args);
}

cmd_t *internal_parse_cmd(const char *line)
{
    cmd_t *cmd = (cmd_t *) malloc(sizeof(cmd_t));
    cmd->input = NULL;
    cmd->output = NULL;

    char *pch[2];
    char *tmp = (char *) malloc((strlen(line) + 1) * sizeof(char));

    strcpy(tmp, line);

    pch[0] = strchr(tmp, '<');
    pch[1] = strchr(tmp, '>');

    /* entrada */
    if(pch[0] != NULL)
    {
        *(pch[0]) = '\0';
    }

    /* saida */
    if(pch[1] != NULL)
    {
        *(pch[1]) = '\0';

        // If the command thrown was >>
        if(*(pch[1] + 1) == '>')
        {
            cmd->cat = 1;
            pch[1]++;
        }
        else
        {
            cmd->cat = 0;
        }
    }

    if(pch[0] != NULL)
    {
        pch[0] = strtok(pch[0] + 1, " ");

        if(pch[0] != NULL)
        {
            cmd->input = (char *) malloc((strlen(pch[0]) + 1) * sizeof(char));
            strcpy(cmd->input, pch[0]);
        }
    }

    if(pch[1] != NULL)
    {
        pch[1] = strtok(pch[1] + 1, " ");

        if(pch[1] != NULL)
        {
            cmd->output = (char *) malloc((strlen(pch[1]) + 1) * sizeof(char));
            strcpy(cmd->output, pch[1]);
        }
    }

    cmd->args = parser_by_char(tmp, ' ', NULL);

    free(tmp);

    return cmd;
}

void internal_free_cmd(cmd_t **cmd)
{
    if(cmd == NULL)
    {
        return;
    }

    if((*cmd)->args != NULL)
    {
        parser_clear(&((*cmd)->args));
    }

    if((*cmd)->input != NULL)
    {
        free((*cmd)->input);
    }

    if((*cmd)->output != NULL)
    {
        free((*cmd)->output);
    }

    free(*cmd);

    *cmd = NULL;
}

int internal_pipe(Status *status, char **cmd_list, int i)   //run
{
    cmd_t *cmd;

    if(i == -1)
    {
        status_set_running(status, 0);
        return 1;
    }
    else if(i == 0)
    {
        cmd = internal_parse_cmd(cmd_list[i]);
        status_set_command(status, cmd->args[0]);


        if(!internal_cmd_builtin(status, cmd))
        {
            internal_cmd_external(status, cmd); //se executar nao volta mais para este local
            internal_free_cmd(&cmd);
            status_set_running(status, 0);
            return 0;

        }

        internal_free_cmd(&cmd);
        status_set_running(status, 0);
        return 1;
    }
    else
    {
        int pid, pipefd[2];

        if(pipe(pipefd) < 0)
        {
            fatal();
        }

        pid = fork();

        if(pid < 0)
        {
            fatal();
        }

        if(pid > 0)
        {
            close(0);
            dup(pipefd[0]);
            close(pipefd[0]);
            close(pipefd[1]);

            cmd = internal_parse_cmd(cmd_list[i]);
            status_set_command(status, cmd->args[0]);

            if(!internal_cmd_builtin(status, cmd))
            {
                internal_cmd_external(status, cmd);
                internal_free_cmd(&cmd);
                status_set_running(status, 0);
                return 0;
            }

            internal_free_cmd(&cmd);
            status_set_running(status, 0);
            return 1;

        }
        else
        {
            close(1);
            dup(pipefd[1]);
            close(pipefd[0]);
            close(pipefd[1]);

            return internal_pipe(status, cmd_list, i - 1);
        }
    }

    return 1;
}

int internal_try_execute(Status *status, char **cmd_list, int i, int flag)
{

    int pid, child_status;
    int job;
    cmd_t *cmd = internal_parse_cmd(cmd_list[0]);

    if(internal_cmd_builtin2(status, cmd))
    {
        internal_free_cmd(&cmd);
        return 1;
    }
    internal_free_cmd(&cmd);

    job = flag & INTERPRETER_NEW_JOB;

    pid = fork();

    if(pid < 0)
    {
        fatal();
    }

    if(pid > 0)
    {
        if(job)
        {
            int i;
            char cmd[STATUS_MAX_CMD_SIZE];
            char *pch = cmd_list[0];
            cmd[0] = '\0';

            for(i = 1; pch != NULL; i++)
            {
                strcat(cmd, pch);
                pch = cmd_list[i];
            }

            strcat(cmd, " &");
            status_add_job(status, pid, cmd);
        }
        else
        {
            status_set_active(status, 0);
            waitpid(pid, &child_status, WSTOPPED);
            status_set_active(status, 1);

            /* Filho foi parado */
            if(WIFSTOPPED(child_status))
            {
                int i;
                char cmd[STATUS_MAX_CMD_SIZE];
                char *pch = cmd_list[0];
                cmd[0] = '\0';

                for(i = 1; pch != NULL; i++)
                {
                    strcat(cmd, pch);
                    pch = cmd_list[i];
                }

                status_add_job(status, pid, cmd);
                status_set_job_status(status, pid, "Stopped");
            }

            tcsetpgrp(STDIN_FILENO, status_get_pid(status));
        }

        return 1;
    }

    signal(SIGCHLD, SIG_DFL);
    signal(SIGINT,  SIG_DFL);
    signal(SIGTSTP, SIG_DFL);
    signal(SIGUSR1, SIG_DFL);
    signal(SIGTSTP, SIG_DFL);
    signal(SIGTTIN, SIG_DFL);
    signal(SIGTTOU, SIG_DFL);

    pid = getpid();

    /* TODO flag de background */
    if(!job)
    {
        tcsetpgrp(STDIN_FILENO, pid);
    }

    setpgid(pid, pid);

    return internal_pipe(status, cmd_list, i);
}

int interpreter(Status *status)
{
    char **cmd_list;
    int ret = 0, size;

    char cmd[STATUS_MAX_CMD_SIZE], *pch;
    char cmd2[STATUS_MAX_CMD_SIZE];

    strcpy(cmd, status_get_command(status));
    strcpy(cmd2, cmd);

    pch = strtok(cmd, "&");

    while(pch != NULL)
    {
        int flag = 0;

        cmd_list = parser_by_char(pch, '|', &size);

        if(cmd2[((long)pch - (long)cmd) + strlen(pch)] == '&')
        {
            flag |= INTERPRETER_NEW_JOB;
        }

        ret = internal_try_execute(status, cmd_list, size - 1, flag);

        if(!ret)
        {
            return ret;
        }

        parser_clear(&cmd_list);

        if(cmd2[((long)pch - (long)cmd) + strlen(pch)] == '\0')
        {
            pch = NULL;
        }
        else
        {
            pch = strtok(pch + strlen(pch) + 1, "&");
        }
    }

    return ret;
}

