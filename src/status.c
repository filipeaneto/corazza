#include "interpreter.h"
#include "status.h"
#include "utils.h"

#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>

struct job_t
{
    int  pid;
    char status[15];
    char cmd[STATUS_MAX_CMD_SIZE];
    short done;
};

struct status_t
{
    char current[STATUS_MAX_CURRENT_SIZE];
    char path[STATUS_MAX_PATH_SIZE];
    char command[STATUS_MAX_CMD_SIZE];

    int last_job;
    int lasts[2];
    struct job_t jobs[STATUS_MAX_JOBS];

    short running;
    short active;

    char message[STATUS_MAX_CMD_SIZE];

    char **args;

    int pgid;

    FILE *err_file;

    int pid;
};

Status *status_init()
{
    Status *status = (Status *) malloc(sizeof(Status));

    status->running = 1;
    status->active = 1;
    strcpy(status->path, getenv("PATH"));

    getcwd(status->current, sizeof(status->current));

    status->args = NULL;

    status->err_file = stderr;

    status->pid = getpid();

    status->last_job = 0;
    status->lasts[0] = -1;
    status->lasts[1] = -1;

    if(setpgid(status->pid, status->pid) < 0)
    {
        fatal();
    }

    tcsetpgrp(fileno(stdin), status->pid);

    return status;
}

void status_destroy(Status **status)
{
    free(*status);
    *status = NULL;
}

const char *status_get_path(const Status *status)
{
    return status->path;
}

const char *status_get_current(const Status *status)
{
    return status->current;
}

void status_set_current(Status *status, const char *current)
{
    if(chdir(current) < 0)
    {
        printf("corazza: %s: No such file or directory\n", current);
    }
    getcwd(status->current, sizeof(status->current));
}

const char *status_get_command(const Status *status)
{
    return status->command;
}

void status_set_command(Status *status, const char *new_cmd)
{
    strcpy(status->command, new_cmd);
}

void status_set_running(Status *status, short running)
{
    status->running = running;
}

short status_is_running(const Status *status)
{
    return status->running;
}

FILE *status_get_err_file(Status *status)
{
    return status->err_file;
}

int status_get_pid(Status *status)
{
    return status->pid;
}

void status_add_job(Status *status, int pid, const char *cmd)
{
    status->jobs[status->last_job].pid = pid;
    status->jobs[status->last_job].done = 0;
    strcpy(status->jobs[status->last_job].cmd, cmd);
    strcpy(status->jobs[status->last_job].status, "Running");

    status->lasts[1] = status->lasts[0];
    status->lasts[0] = status->last_job;
    status->last_job++;

    printf("[%d] %d\n", status->last_job, pid);
}

void status_print_jobs(Status *status, FILE *fp)
{
    int i, j = status->last_job;

    for(i = 0; i < j; i++)
    {
        if(status->jobs[i].done == 1)
        {
            continue;
        }
        fprintf(fp, "[%d]%c %s\t\t %s\n", i+1,
                (status->lasts[0] == i) ? '+' : 
                (status->lasts[1] == i) ? '-' : ' ',
                status->jobs[i].status, status->jobs[i].cmd);
    }
}

int status_get_next_job(Status *status, int i)
{
    static int last = 0;

    if(i == 0)
        i = ++last;
    else
        last = i;

    if(i > status->last_job)
        return 0;

    short done = status->jobs[i-1].done;

    return !done ? status->jobs[i-1].pid : status_get_next_job(status, 0);
}

void status_set_active(Status *status, short active)
{
    status->active = active;
}

short status_is_active(const Status *status)
{
    return status->active;
}

void status_set_job_status(Status *status, int pid, const char *job_status)
{
    int i;
    for(i = 0; i < status->last_job; i++)
    {
        if(status->jobs[i].pid == pid)
        {
            strcpy(status->jobs[i].status, job_status);
            break;
        }
    }
}

void status_finish_job(Status *status, int pid)
{
    int i;
    for(i = 0; i < status->last_job; i++)
    {
        if(status->jobs[i].pid == pid && !status->jobs[i].done)
        {
            strcpy(status->jobs[i].status, "Done");
            status->jobs[i].done = 1;
            sprintf(status->message, "[%d]%c %s\t\t %s\n", i+1,
                (status->lasts[0] == i) ? '+' : 
                (status->lasts[1] == i) ? '-' : ' ',
                status->jobs[i].status, status->jobs[i].cmd);
            break;
        }
    }
}

void status_job_background(Status *status, int index)
{
    int i = index - 1;

    if(i >= status->last_job || status->jobs[i].done)
    {
        printf("corazza: bg: %d: no such job\n", index);
        return;
    }

    if(strcmp(status->jobs[i].status, "Stopped") == 0)
    {
        status->lasts[1] = status->lasts[0];
        status->lasts[0] = i;

        printf("[%d]%c %s\n", i+1,
                (status->lasts[0] == i) ? '+' : 
                (status->lasts[1] == i) ? '-' : ' ',
                status->jobs[i].cmd);

        if(kill(status->jobs[i].pid, SIGCONT) < 0)
        {
            fatal();
        }
    }
    else if(!status->jobs[i].done)
    {
        printf("corazza: bg: job %d already in background\n", index);
    }
}

int status_job_foreground(Status *status, int index)
{
    int i = index - 1;

    if(i >= status->last_job || status->jobs[i].done)
    {
        printf("corazza: bg: %d: no such job\n", index);
        return 0;
    }

    if(strcmp(status->jobs[i].status, "Stopped") == 0)
    {
        if(kill(status->jobs[i].pid, SIGCONT) < 0)
        {
            fatal();
        }
    }

    printf("%s\n", status->jobs[i].cmd);

    return status->jobs[i].pid;
}

void status_print_message(Status *status)
{
    printf("%s", status->message);
    status->message[0] = '\0';
}

