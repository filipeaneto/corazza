#include "parser.h"

#include <stdlib.h>
#include <string.h>

int internal_count_char(const char *c, char symbol)
{
    int count = 0;
    char *pch;
    char *last;

    pch = strchr(c, symbol);
    last = pch;

    /* no inicio */
    if(pch == c)
    {
        count--;
    }

    while(pch != NULL)
    {
        last = pch;
        pch = strchr(last + 1, symbol);

        /* conta __ como _ */
        if(last != pch - 1)
        {
            count++;
        }
    }

    /* termina com espaco */
    if(last == c + strlen(c) - 1)
    {
        count--;
    }

    return count;
}

void parser_clear(char ***ppch)
{
    if(*ppch != NULL)
    {
        int j = 0;

        while((*ppch)[j] != NULL)
        {
            free((*ppch)[j++]);
        }

        free(*ppch);
    }

    *ppch = NULL;
}

char **parser_by_char(const char *_word, char symbol, int *size)
{
    int count = 0, i = 0;
    char token[] = {symbol, '\0'};

    char **parsed;
    char *word;
    char *pch;

    word = (char *) malloc((strlen(_word) + 1) * sizeof(char));

    strcpy(word, _word);

    count = internal_count_char(word, symbol);

    parsed = (char **) malloc((count + 2) * sizeof(char *));

    pch = strtok(word, token);

    while(pch != NULL)
    {
        parsed[i] = (char *) malloc((strlen(pch) + 1) * sizeof(char));
        strcpy(parsed[i++], pch);

        pch = strtok(NULL, token);
    }

    parsed[i] = NULL;

    if(size != NULL)
    {
        *size = i;
    }

    free(word);

    return parsed;
}

