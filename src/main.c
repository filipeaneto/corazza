/*
 * File:   main.c
 * Author: lucao
 *
 * Created on August 27, 2012, 11:05 AM
 */

#include "interpreter.h"
#include "status.h"
#include "handler.h"

#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>

char *trim(char *str)
{
    char *end;

    while (isspace(*str))
    {
        str++;
    }

    if (*str == 0)
    {
        return str;
    }

    end = str + strlen(str) - 1;
    while(end > str && isspace(*end))
    {
        end--;
    }

    *(end+1) = '\0';

    return str;
}

int main(int argc, char **argv)
{
    Status *status;
    status = status_init();

    (void) argc;
    (void) argv;

    /* Inicializa sinais */
    handler_set_status(status);
    signal(SIGCHLD, chandler);
    signal(SIGINT, inthandler);
    signal(SIGTSTP, stphandler);
    signal(SIGTTIN, SIG_IGN);
    signal(SIGTTOU, SIG_IGN);

    char buffer_cmd[STATUS_MAX_CMD_SIZE] = "\0";

    while(status_is_running(status))
    {
        fflush(stdout);
        status_print_message(status);
        printf("[%s]$ ", status_get_current(status)); /* Prompt */

        if (fgets(buffer_cmd, STATUS_MAX_CMD_SIZE, stdin) == NULL)
        {
            break;
        }

        char *tmp_cmd = trim(buffer_cmd);
        if(tmp_cmd[0] == '\0')
        {
            continue;
        }

        status_set_command(status, tmp_cmd);

        if(!interpreter(status))    /* Try to execute built in */
        {
            printf("corazza: %s: command not found...\n",
                    status_get_command(status));
        }
    }

    status_destroy(&status);

    return (EXIT_SUCCESS);
}



