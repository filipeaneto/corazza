# Compiler and Linker Command
CC = gcc

# Compiler Settings
INCLUDES = ./include
CFLAGS := -I$(INCLUDES) -Wall -Wextra -O2

# Linker Settings
LIBS =
LDFLAGS := $(LIBS)

# Sources
SDIR = src
SOURCES := $(shell ls $(SDIR)/*.c)

# Objects
ODIR = obj
_OBJ = $(SOURCES:.c=.o)
OBJ = $(patsubst $(SDIR)/%,$(ODIR)/%,$(_OBJ))

# Dependencies
DEPS := $(shell ls $(INCLUDES)/*.h)

all: $(ODIR) corazza

$(ODIR):
	mkdir $(ODIR)

corazza: $(OBJ)
	$(CC) $(LDFLAGS) $(OBJ) -o corazza

$(ODIR)/%.o: $(SDIR)/%.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

clean:
	@echo "Cleaning..."
	- @rm -f $(ODIR)/*.o
