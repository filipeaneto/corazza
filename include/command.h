 /*
 * File:   command.h
 * Author: filipeaneto
 *
 * Created on Sep 9, 2012, 10:28 AM
 */

#ifndef COMMAND_H
#define COMMAND_H

#include "status.h"

#define COMMAND_EXIT    "exit"
#define COMMAND_HELP    "help"
#define COMMAND_JOBS    "jobs"
#define COMMAND_CD      "cd"
#define COMMAND_BG      "bg"
#define COMMAND_FG      "fg"

char *cmd_help(Status *status);
void cmd_exit(Status *status);

#endif  /* COMMAND_H */


