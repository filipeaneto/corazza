/*
 * File:   status.h
 * Author: lucao
 *
 * Created on August 27, 2012, 11:37 AM
 */

#ifndef STATUS_H
#define STATUS_H

#include <stdio.h>

#define STATUS_MAX_PATH_SIZE    1024
#define STATUS_MAX_CMD_SIZE     512
#define STATUS_MAX_CURRENT_SIZE 256
#define STATUS_MAX_JOBS         32

typedef struct status_t Status;

Status *status_init();
void status_destroy(Status **status);

const char *status_get_path(const Status *status);

const char *status_get_current(const Status *status);
void status_set_current(Status *status, const char *current);

const char *status_get_command(const Status *status);
void status_set_command(Status *status, const char *new_cmd);

void status_set_running(Status *status, short running);
short status_is_running(const Status *status);

void status_set_active(Status *status, short active);
short status_is_active(const Status *status);

FILE *status_get_err_file(Status *status);

int status_get_pid(Status *status);

void status_add_job(Status *status, int pid, const char *cmd);
int status_get_next_job(Status *status, int i);
void status_print_jobs(Status *status, FILE *fp);
void status_set_job_status(Status *status, int pid, const char *job_status);
void status_finish_job(Status *status, int pid);
void status_job_background(Status *status, int index);
int status_job_foreground(Status *status, int index);

void status_print_message(Status *status);

#endif  /* STATUS_H */



