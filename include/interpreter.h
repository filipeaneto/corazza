/*
 * File:   interpreter.h
 * Author: lucao
 *
 * Created on August 27, 2012, 11:30 AM
 */

#ifndef INTERPRETER_H
#define INTERPRETER_H

#define INTERPRETER_NEW_JOB     1

#include "status.h"

int interpreter(Status *status);

#endif  /* INTERPRETER_H */

/*

ls & ls & ls0
ls & ls & ls0

v
ls 0 ls & ls0
^
ls & ls & ls0
---^
   
v
ls 0 ls 0 ls0
++++^
ls & ls & ls0
++++^---^

v
ls 0 ls 0 ls0
+++++++++^
ls & ls & ls0
+++++++++^--^

*/
