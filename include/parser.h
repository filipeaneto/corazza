#ifndef PARSER_H
#define PARSER_H


void parser_clear(char ***ppch);
char **parser_by_char(const char *word, char symbol, int *size);


#endif  /* PARSER_H */




